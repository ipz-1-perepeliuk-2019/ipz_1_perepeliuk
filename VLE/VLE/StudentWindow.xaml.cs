﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VLE.Models;
using VLE.DataAccess.Repositories;

namespace VLE
{
    /// <summary>
    /// Interaction logic for BookWindow.xaml
    /// </summary>
    public partial class StudentWindow : Window
    {
        private Client Client { get; set; }
        public StudentWindow(Client client)
        {
            InitializeComponent();
            Client = client;
            using (VleContext context = new VleContext())
            {
                var courses = context.Courses.ToList();

                foreach (Course course in courses)
                {
                    CourseBlock.Text += "Name: ";
                    CourseBlock.Text += course.Name;
                    CourseBlock.Text += "\nAuthor: ";
                    CourseBlock.Text += course.Author;
                    CourseBlock.Text += "\nInformation: ";
                    CourseBlock.Text += course.Text;
                    CourseBlock.Text += "\n";
                }
            }
        }
        private void Back_Click(object sender, RoutedEventArgs e)
        {
            LogIn main = new LogIn();
            main.Show();
            Close();
        }
    }
}
